<?php
	
	include('connect_db.php');
	include('function.php');

	if (!isset($_SESSION['user_id'])) header('location: /login.php');
	
	$report_id = $_GET['report_id'];
	if (!is_numeric($report_id)) die();
	
	$sql_report = $mysqli->query("SELECT * FROM `report` WHERE `report_id`=$report_id");
	$res_report = $sql_report->fetch_array();
	
	$report_name = $res_report['name'];
	
	$array_merchant = get_array_merchant($report_id,$mysqli);
	
?>
<!DOCTYPE html>
<html>
	<head> 
		<title><?=$report_name;?></title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<META NAME="description" CONTENT=""/>
		<link rel="SHORTCUT ICON" href="images/1.ico"type="image/x-icon"/>
		<META Name="keywords" Content=""/>
		<meta http-equiv="Content-Language" content="ru-RU"/>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<link rel="stylesheet" href="/style.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	</head>

	<body>
	

		<?php include('header.php'); ?>	
		
		
		<div class="conteiner-fluid p-4">
		
			
			<h4><?=$report_name;?></h4>
			<br/>
			<table class="table table_report">
				<thead>
					<tr>
						<th style="width: 300px;">ON</th>
						<th style="width: 200px;">CN</th>
						<th>AS</th>
						<th>SIM1</th>
						<th>Расход SIM1 (руб)</th>
						<th>SIM2</th>
						<th>Расход SIM2 (руб)</th>
						<th>Общий расход</th>
					</tr>
				</thead>
				<tbody>
				
				<?php

					
					for ($i = 0; $i < count($array_merchant); $i++) {	
						
						$as = $array_merchant[$i]['as'];
						$on = $array_merchant[$i]['on'];
						$cn = $array_merchant[$i]['cn'];
						$sim1 = $array_merchant[$i]['sim1'];
						$sim2 = $array_merchant[$i]['sim2'];
						$sim1_summ = $array_merchant[$i]['sim1_summ'];
						$sim2_summ = $array_merchant[$i]['sim2_summ'];
						$total_summ = $array_merchant[$i]['total_summ'];
						$total_summ = $total_summ. ' руб';
						
				?>
						<tr>
							<td><?=$on;?></td>
							<td><?=$cn;?></td>
							<td><?=$as;?></td>
							<td><?=$sim1;?></td>
							<td><?=$sim1_summ;?></td>
							<td><?=$sim2;?></td>
							<td><?=$sim2_summ;?></td>
							<td><?=$total_summ;?></td>
						</tr>
				<?php
						
					}
				?>
				
				</tbody>
			</table>
				
		
			
		</div>
		
		<?php include('footer.php'); ?>	

	</body>
</html>

		
		