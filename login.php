<?php
	include('connect_db.php');
	
?>
<!DOCTYPE html>
<html>
	<head> 
		<title>Авторизация</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<META NAME="description" CONTENT=""/>
		<link rel="SHORTCUT ICON" href="images/1.ico"type="image/x-icon"/>
		<META Name="keywords" Content=""/>
		<meta http-equiv="Content-Language" content="ru-RU"/>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<link rel="stylesheet" href="/style.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	</head>

	<body>
		
		<div class="conteiner-fluid p-4">
		
			<?php if ($mist != "") echo "<div class='alert alert-danger'>$mist</div>";?>
			
			<div class="form_enter_to_cite">
				
				<h4>Вход на сайт</h4> 
				
				
				<form action="" method='POST' class='form-group'>
					
					<div class="mt-2">
						<input type="text" class="form-control"value="" placeholder="Логин" name="login"/>
					</div>
					<div class="mt-2">
						<input type="password" class="form-control" value="" placeholder="Пароль" name="pass"/>
					</div>
					<div class="mt-2">	
						<input type="submit" class="form-control btn btn-success" value="Войти" name="sb_enter"/>
					</div>
					
				</form>
			</div>
			
		
			
		</div>
		
	</body>
</html>

		
		