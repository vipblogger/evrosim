<?php
	
	include('connect_db.php');
	include('function.php');

	if (!isset($_SESSION['user_id'])) header('location: /login.php');
	
	$report_id = $_GET['report_id'];
	if (!is_numeric($report_id)) die();
	
	$sql_report = $mysqli->query("SELECT * FROM `report` WHERE `report_id`=$report_id");
	$res_report = $sql_report->fetch_array();
	
	$report_name = $res_report['name'];
	
	$array_staff = get_array_staff($report_id,$mysqli);
	
	/*echo "<pre>";
	print_r($array_staff);
	echo "</pre>";*/
	
?>
<!DOCTYPE html>
<html>
	<head> 
		<title><?=$report_name;?></title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<META NAME="description" CONTENT=""/>
		<link rel="SHORTCUT ICON" href="images/1.ico"type="image/x-icon"/>
		<META Name="keywords" Content=""/>
		<meta http-equiv="Content-Language" content="ru-RU"/>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<link rel="stylesheet" href="/style.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	</head>

	<body>
	

		<?php include('header.php'); ?>	
		
		
		<div class="conteiner-fluid p-4">
		
			
			<h4><?=$report_name;?></h4>
			<br/>
			<p><u>Общая сумма расходов</u>: <strong><?=$array_staff['total_summ'];?></strong> руб</p>
			<br/>
			<table class="table table_report">
				<thead>
					<tr>
						<th style="width: 300px;">Сотрудник</th>
						<th style="width: 200px;">Номер телефона</th>
						<th>Расход</th>
					</tr>
				</thead>
				<tbody>
				
				<?php

					
					for ($i = 0; $i < count($array_staff['data']); $i++) {	
						
						$name = $array_staff['data'][$i]['name'];
						$phone = $array_staff['data'][$i]['phone'];
						$summ = $array_staff['data'][$i]['summ'];

				?>
						<tr>
							<td><?=$name;?></td>
							<td><?=$phone;?></td>
							<td><?=$summ;?></td>

						</tr>
				<?php
						
					}
				?>
				
				</tbody>
			</table>
				
		
			
		</div>
		
		<?php include('footer.php'); ?>	

	</body>
</html>

		
		