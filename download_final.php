<?php
	
	include('connect_db.php');
	include('function.php');
	
	require_once 'Classes/PHPExcel/IOFactory.php';
	require_once 'Classes/PHPExcel/DocumentProperties.php';
	require_once 'Classes/PHPExcel/Cell.php';
	
	if (!isset($_SESSION['user_id'])) header('location: /login.php');
	
	$report_id = $_GET['report_id'];
	if (!is_numeric($report_id)) die();
	
	$sql_report = $mysqli->query("SELECT * FROM `report` WHERE `report_id`=$report_id");
	$res_report = $sql_report->fetch_array();
	
	if ($res_report['excel_final'] != "") {
		header('location: '.$res_report['excel_final']);
		die();
	}
	
	$report_name = $res_report['name'];
	
	$array_merchant = get_array_merchant_full($report_id,$mysqli);
		
	$uniqid = uniqid();
	$file_name = $_SERVER['DOCUMENT_ROOT']."/excel_report/report_f_".$uniqid.".xlsx";
	$only_file_name = "report_f_".$uniqid.".xlsx";
	
	
	$objPHPExcel = PHPExcel_IOFactory::load("report_template_final.xlsx");

	$objPHPExcel->setActiveSheetIndex(6);
	
	$total_summ_merchant = 0;
	
	$row = 2;
	
	for ($i = 0; $i < count($array_merchant); $i++) {
		
		$as = $array_merchant[$i]['as'];
		$on = $array_merchant[$i]['on'];
		$cn = $array_merchant[$i]['cn'];
		$sim1 = $array_merchant[$i]['sim1'];
		$sim2 = $array_merchant[$i]['sim2'];
		$sim1_summ = $array_merchant[$i]['sim1_summ'];
		$sim2_summ = $array_merchant[$i]['sim2_summ'];
		$total_summ = $array_merchant[$i]['total_summ'];
		
		
		
		$total_summ_merchant = $total_summ_merchant + $total_summ;
		
		$objPHPExcel->getActiveSheet()->getStyle("C".$row)
					  ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	
		$objPHPExcel->getActiveSheet()->getStyle("D".$row)
					  ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	
		$objPHPExcel->getActiveSheet()->getStyle("E".$row)
					  ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	
		$objPHPExcel->getActiveSheet()->getStyle("F".$row)
					  ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	
		$objPHPExcel->getActiveSheet()->getStyle("G".$row)
					  ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	
		$objPHPExcel->getActiveSheet()->getStyle("H".$row)
					  ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	
		$objPHPExcel->getActiveSheet()->getStyle("A".$row)
									  ->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->getStyle("B".$row)
									  ->getAlignment()->setWrapText(true);

		if ($sim1 == 0) {
			$sim1 = "";
			$sim1_summ = "";
		}
		
		if ($sim2 == 0) {
			$sim2 = "";
			$sim2_summ = "";
		}
		
		$objPHPExcel->getActiveSheet()
					->setCellValue('A'.$row, $on)
					->setCellValue('B'.$row, $cn)
					->setCellValue('C'.$row, $as)
					->setCellValue('D'.$row, $sim1)
					->setCellValue('E'.$row, $sim1_summ)
					->setCellValue('F'.$row, $sim2)
					->setCellValue('G'.$row, $sim2_summ)
					->setCellValue('H'.$row, $total_summ);
					
		
		$row++;
	}
	
	
	
	$array_not_isset_in_merchant = find_not_isset_in_merchant($report_id,$mysqli);
	
	for ($i = 0; $i < count($array_not_isset_in_merchant); $i++) {	
		
		$phone = $array_not_isset_in_merchant[$i]['phone'];
		$summ = $array_not_isset_in_merchant[$i]['summ'];
		
		$objPHPExcel->getActiveSheet()->getStyle("C".$row)
					  ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$objPHPExcel->getActiveSheet()->getStyle("A".$row)
					  ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

	
		$objPHPExcel->getActiveSheet()->getStyle("D".$row)
					  ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$objPHPExcel->getActiveSheet()->getStyle("E".$row)
					  ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$objPHPExcel->getActiveSheet()->getStyle("H".$row)
					  ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	
		$objPHPExcel->getActiveSheet()
					->setCellValue('A'.$row, $phone)
					->setCellValue('C'.$row, "есть только у оператора")
					->setCellValue('E'.$row, $summ)
					->setCellValue('H'.$row, $summ)
					->setCellValue('D'.$row, $phone);
		
		$row++;
		
	}
	
	$total_ob_pr = "";
	$total_staff_pr = "";
	if ($res_report['month'] != "") {
		
		$name_month = get_name_month_by_num($res_report['month']);
		
		$pr_month = $res_report['month'] - 1;
		
		if ($pr_month == 0) {	
			$pr_month = 12;
			$pr_year = $res_report['year'] - 1;
		} else {
			$pr_year = $res_report['year'];
		}
		
		$name_pr_month = get_name_month_by_num($pr_month);
		
		$objPHPExcel->setActiveSheetIndex(0);
		
		$objPHPExcel->getActiveSheet()
					->setCellValue("A1", $name_month);
					
		$sql_pr_summ = $mysqli->query("SELECT * FROM `report` WHERE `month`=$pr_month and `year`=$pr_year and `total_staff` NOT LIKE '0' ORDER BY `report_id` desc LIMIT 1");
		$col_pr_summ = $sql_pr_summ->num_rows;
		if ($col_pr_summ > 0) {
			$res_pr_summ = $sql_pr_summ->fetch_array();
			$report_id_pr = $res_pr_summ['report_id'];
			$sql_pr_rf = $mysqli->query("SELECT * FROM `report_file` WHERE `report_id`=$report_id_pr and `operator_id`=3");
			$col_pr_rf = $sql_pr_rf->num_rows;
			if ($col_pr_rf > 0) {
				$res_rf_summ = $sql_pr_rf->fetch_array();
				$total_pr_summ = $res_rf_summ['total_summ'];
				$total_ob_pr = $res_rf_summ['total_ob'];
				$total_staff_pr = $total_pr_summ - $total_ob_pr;
			}
		} 
	}
	
	if ($total_ob_pr != "" and $total_ob_pr != 0) {
		$total_ob_pr = "$name_pr_month ".$total_ob_pr."; ";
	}
	
	if ($total_staff_pr != 0 and $total_staff_pr != '') {
		$total_staff_pr = "$name_pr_month ".$total_staff_pr."; ";
	}
	
	$total_ob = 0;
	$total_staff = 0;
	
	
	$sql_report_file = $mysqli->query("SELECT * FROM `report_file` WHERE `report_id`=$report_id");
	while ($res_report_file = $sql_report_file->fetch_assoc()) {
		
		$operator_id = $res_report_file['operator_id'];
		
		$sql_operator = $mysqli->query("SELECT * FROM `operator` WHERE `operator_id`=$operator_id");
		$res_operator = $sql_operator->fetch_array();
		
		
		$sheet_index = $res_operator['sheet_index'];
		
		$objPHPExcel->setActiveSheetIndex($sheet_index);
		
		if ($operator_id == 5) {
		
			$array_operator = get_array_staff($report_id,$mysqli);
			if (!isset($array_operator['data'])) continue;
			$row = 3;
			for ($i = 0; $i < count($array_operator['data']); $i++) {	
				
				$objPHPExcel->getActiveSheet()
							->setCellValue('A'.$row, $array_operator['data'][$i]['phone_cell'])
							->setCellValue('B'.$row, $array_operator['data'][$i]['name'])
							->setCellValue('C'.$row, $array_operator['data'][$i]['D'])
							->setCellValue('D'.$row, $array_operator['data'][$i]['F'])
							->setCellValue('E'.$row, $array_operator['data'][$i]['H'])
							->setCellValue('F'.$row, $array_operator['data'][$i]['J'])
							->setCellValue('G'.$row, $array_operator['data'][$i]['N'])
							->setCellValue('H'.$row, $array_operator['data'][$i]['S'])
							->setCellValue('I'.$row, $array_operator['data'][$i]['X'])
							->setCellValue('J'.$row, $array_operator['data'][$i]['AC'])
							->setCellValue('K'.$row, $array_operator['data'][$i]['AE'])
							->setCellValue('M'.$row, $array_operator['data'][$i]['AI_2'])
							->setCellValue('N'.$row, $array_operator['data'][$i]['AK'])
							->setCellValue('P'.$row, $array_operator['data'][$i]['AO'])
							->setCellValue('Q'.$row, $array_operator['data'][$i]['AQ'])
							->setCellValue('R'.$row, $array_operator['data'][$i]['AT'])
							->setCellValue('S'.$row, $array_operator['data'][$i]['AV'])
							->setCellValue('T'.$row, $array_operator['data'][$i]['AX'])
							->setCellValue('U'.$row, $array_operator['data'][$i]['BA'])
							->setCellValue('V'.$row, $array_operator['data'][$i]['BC']);
				
				$row++;
				
			}

			$objPHPExcel->setActiveSheetIndex(0);
			// ПОМЕНЯЛ
			
			
			
			$objPHPExcel->getActiveSheet()
						->setCellValue("D9", "Сотрудники $total_staff_pr $name_month ".$array_operator['total_summ']);
			
			$sql_rf = $mysqli->query("SELECT * FROM `report_file` WHERE `report_id`=$report_id and `operator_id`=3");
			$col_rf = $sql_rf->num_rows;
			if ($col_rf > 0) {
				$res_rf = $sql_rf->fetch_array();
				$total_summ_megafon = $res_rf['total_summ'];
				$total_ob_megafon = $res_rf['total_ob'];
				$total_staff_megafon = $total_summ_megafon - $total_ob_megafon;
			}
			
			$total = $total_summ_megafon;
			$total_staff = $total_staff_megafon;
			$total_ob = $total_ob_megafon;
			
			$objPHPExcel->getActiveSheet()
						->setCellValue("D8", "По объектам $total_ob_pr $name_month ".$total_ob);
			
			
		} else {
		
			$array_operator = get_operator_merchant($report_id,$operator_id,$mysqli);
			if (!isset($array_operator['data'])) continue;
			$row = 2;
			for ($i = 0; $i < count($array_operator['data']); $i++) {	
				
				$objPHPExcel->getActiveSheet()
							->setCellValue('A'.$row, $array_operator['data'][$i]['phone'])
							->setCellValue('B'.$row, $array_operator['data'][$i]['summ'])
							->setCellValue('C'.$row, $array_operator['data'][$i]['on'])
							->setCellValue('D'.$row, $array_operator['data'][$i]['cn']);
				
				$row++;
				
			}
			
			$objPHPExcel->setActiveSheetIndex(0);
			
			$objPHPExcel->getActiveSheet()
						->setCellValue($res_operator['total_place']."7", $array_operator['total_summ']);
			
			if ($res_report['month'] != "") {
				
				$objPHPExcel->getActiveSheet()
							->setCellValue($res_operator['total_place']."6", $name_month);
			
				
				$sql_get_pr_month = $mysqli->query("SELECT * FROM `report` WHERE `month`=$pr_month and `year`=$pr_year and `total_staff` NOT LIKE '0' ORDER BY `report_id` desc");
				$col_get_pr_month = $sql_get_pr_month->num_rows;
				if ($col_get_pr_month > 0) {
					
					$res_get_pr_month = $sql_get_pr_month->fetch_array();
					$report_id_pr = $res_get_pr_month['report_id'];
					
					$sql_pr_operator = $mysqli->query("SELECT * FROM `report_file` WHERE `report_id`=$report_id_pr and `operator_id`=$operator_id");
					$col_pr_operator = $sql_pr_operator->num_rows;

					if ($col_pr_operator > 0) {
						
						$res_pr_operator = $sql_pr_operator->fetch_array();

						$objPHPExcel->getActiveSheet()
									->setCellValue($res_operator['total_place_pr']."7", $res_pr_operator['total_summ']);
						
						$objPHPExcel->getActiveSheet()
									->setCellValue($res_operator['total_place_pr']."6", $name_pr_month);
			
			
					}
					
				}
			}
		}
		
		
	}
	
	// Сохраняем данные по созданному отчету
	// total_ob - расходы по объекту, потом использвется для вставки данных по расходу в этом месяце
	// total_staff - расходы по сотрудникам, потом использвется для вставки данных по расходу в этом месяце
	// excel_final - путь до созданного месячного отчета. Если отчет создан, дальше просто печатается
	
	$mysqli->query("
		UPDATE `report` SET 
		`excel_final`='/excel_report/$only_file_name',
		`total_ob`='$total_ob',
		`total_staff`='$total_staff'
		WHERE `report_id`=$report_id
	");
	
	
	
	

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save($file_name); 
	
	
	// Очищаем данные, которые использоваелиь для формирования отчета (чтобы беречь место на диске)
	// Пока уберём очищение, дебажим
	// $mysqli->query("DELETE FROM `merchant` WHERE `report_id`=$report_id");
	// $mysqli->query("DELETE FROM `data` WHERE `report_id`=$report_id");
	// $mysqli->query("DELETE FROM `data_megafon` WHERE `report_id`=$report_id");
	// $mysqli->query("DELETE FROM `staff` WHERE `report_id`=$report_id");
	
	// Выводим на печать файл
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.$only_file_name.'"');
	header('Cache-Control: max-age=0');
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output'); 
	exit;
	
	
	
	
	
?>
		
		