<?php
	
	include('connect_db.php');
	include('function.php');
	
	require_once 'Classes/PHPExcel/IOFactory.php';
	require_once 'Classes/PHPExcel/DocumentProperties.php';
	require_once 'Classes/PHPExcel/Cell.php';
	
	if (!isset($_SESSION['user_id'])) header('location: /login.php');
	
	$report_id = $_GET['report_id'];
	if (!is_numeric($report_id)) die();
	
	$sql_report = $mysqli->query("SELECT * FROM `report` WHERE `report_id`=$report_id");
	$res_report = $sql_report->fetch_array();
	
	$report_name = $res_report['name'];
	
	$array_merchant = get_array_merchant_block($report_id,$mysqli);
		
	$uniqid = uniqid();
	$file_name = $_SERVER['DOCUMENT_ROOT']."/excel_report/report_".$uniqid.".xlsx";
	$only_file_name = "report_".$uniqid.".xlsx";
	
	$objPHPExcel = PHPExcel_IOFactory::load("report_template_week.xlsx");

	$row = 2;
	for ($i = 0; $i < count($array_merchant); $i++) {	
		
		$as = $array_merchant[$i]['as'];
		$on = $array_merchant[$i]['on'];
		$cn = $array_merchant[$i]['cn'];
		$phone = $array_merchant[$i]['phone'];
		$summ = $array_merchant[$i]['summ'];
		$operator = $array_merchant[$i]['operator'];
		
		$objPHPExcel->getActiveSheet()->getStyle("C".$row)
					  ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	
		$objPHPExcel->getActiveSheet()->getStyle("D".$row)
					  ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	
		$objPHPExcel->getActiveSheet()->getStyle("E".$row)
					  ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	
		$objPHPExcel->getActiveSheet()->getStyle("F".$row)
					  ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$objPHPExcel->getActiveSheet()->getStyle("A".$row)
									  ->getAlignment()->setWrapText(true);

		$objPHPExcel->getActiveSheet()->getStyle("B".$row)
									  ->getAlignment()->setWrapText(true);

		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$row, $on)
					->setCellValue('B'.$row, $cn)
					->setCellValue('C'.$row, $as)
					->setCellValue('D'.$row, $phone)
					->setCellValue('E'.$row, $operator)
					->setCellValue('F'.$row, $summ);
					
		
		$row++;
	}
	

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save($file_name); 
	
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.$only_file_name.'"');
	header('Cache-Control: max-age=0');
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output'); 
	exit;
	
	
	
?>
		
		