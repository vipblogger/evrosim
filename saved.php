<?php
	include('connect_db.php');
	include('function.php');
	
	if (!isset($_SESSION['user_id'])) header('location: /login.php');
	
	if (isset($_GET['del']) and is_numeric($_GET['del'])) {
		$mysqli->query("DELETE FROM `report` WHERE `report_id`=".$_GET['del']);
		$mysqli->query("DELETE FROM `report_file` WHERE `report_id`=".$_GET['del']);
		$mysqli->query("DELETE FROM `data` WHERE `report_id`=".$_GET['del']);
	}
?>
<!DOCTYPE html>
<html>
	<head> 
		<title>Сохраненные отчеты EXEL</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<META NAME="description" CONTENT=""/>
		<link rel="SHORTCUT ICON" href="images/1.ico"type="image/x-icon"/>
		<META Name="keywords" Content=""/>
		<meta http-equiv="Content-Language" content="ru-RU"/>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<link rel="stylesheet" href="/style.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	</head>

	<body>
		<?php include('header.php'); ?>	
		
		
		<div class="conteiner-fluid p-4">
		
			<h4>Сформированные отчеты</h4>
			<br/>
			<table class="table">
				<thead>
					<tr>
						<th>Дата</th>
						<th>Название отчета</th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					
					
				<?php
					$sql_report = $mysqli->query("SELECT * FROM `report` ORDER BY `report_id` desc");
					while ($res_report = $sql_report->fetch_assoc()) {
						$report_id = $res_report['report_id'];
				?>
						<tr>
							<td><?=date('d.m.Y в H:i', $res_report['datetime']);?></td>
							<td>
								<div class="pb-1"><?=$res_report['name'];?></div>
								<?php
									$sql_report_file = $mysqli->query("SELECT * FROM `report_file` WHERE `report_id`=$report_id");
									while ($res_report_file = $sql_report_file->fetch_assoc()) {
										
										$operator_id = $res_report_file['operator_id'];
										$sql_operator = $mysqli->query("SELECT * FROM `operator` WHERE `operator_id`=$operator_id");
										$res_operator = $sql_operator->fetch_array();
								?>
											
										<div style="font-size: 12px; color: rgb(100,100,100);"><?=$res_operator['name'];?>: <a href="/<?=$res_report_file['src'];?>"><?=$res_report_file['src'];?></a></div>
											
								<?php	
									}
								?>
								
							</td>
							<td><a href="report.php?report_id=<?=$res_report['report_id'];?>">[&nbsp;смотреть&nbsp;отчет&nbsp;]</a></td>
							<td><a href="download.php?report_id=<?=$res_report['report_id'];?>">[&nbsp;скачать&nbsp;отчет&nbsp;]</a></td>
							<td><a style="color: red;" href="saved.php?del=<?=$res_report['report_id'];?>">[&nbsp;удалить&nbsp;]</a></td>
						</tr>
				<?php
						
					}
				?>
				
				</tbody>
			</table>
				
		
			
		</div>
		
		<?php include('footer.php'); ?>	

	</body>
</html>

		
		