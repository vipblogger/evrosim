<?php
	
	function to_db($text,$mysqli) {
		
		$text = htmlspecialchars($text);
		$text = addslashes($text);
		$text = $mysqli->real_escape_string($text);
		
		return $text;
	}
	
	function getExtension($filename) {
		$arr = explode(".", $filename);
		return $arr[count($arr) - 1];
	}
	
	
	function update_json($mysqli, $report_id, $not_update = false) {

	    if ($not_update) {
            $mysqli->query("
				UPDATE `merchant` SET
				`report_id`='$report_id'
				WHERE report_id = (SELECT MAX(report_id) FROM report WHERE 1)
			");
	        return;
        }

		$post = [];
		$ch = curl_init('http://evroadmin.ru/engine/getFreelanceJSON');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		$response = curl_exec($ch);
		curl_close($ch);
		$res = json_decode($response,1);

		foreach ($res as $key=>$value) {
			
			if (isset($value['on'])) $on = $value['on'];
			else $on = "";
			
			if (isset($value['cn'])) $cn = $value['cn'];
			else $cn = "";
			
			if (isset($value['s1'])) $s1 = $value['s1'];
			else $s1 = 0;
			
			if (isset($value['s2'])) $s2 = $value['s2'];
			else $s2 = 0;
			
			if (isset($value['as'])) $as = $value['as'];
			else $as = "";
			
			$on = to_db($on,$mysqli);
			$cn = to_db($cn,$mysqli);
			$s1 = to_db($s1,$mysqli);
			$s2 = to_db($s2,$mysqli);
			$as = to_db($as,$mysqli);
			
			
			$mysqli->query("
				INSERT INTO `merchant` SET
				`on`='$on',
				`cn`='$cn',
				`s1`='$s1',
				`s2`='$s2',
				`as`='$as',
				`report_id`='$report_id'
			");
			
			
		}
	}
	
	
	// МТС
	function file_load_1($file_id,$mysqli,$objPHPExcel,$report_id,$operator_id, $type) { 
		
		$total_summ = 0;
		
		if ($type == 0) { // месячный отчет
		
			foreach ($objPHPExcel as $row=>$data) {
				
				$phone = $data['A'];
				$phone = substr($phone,1,strlen($phone));
				if (!is_numeric($phone)) continue;
				$summ = $data['D'];
				$summ = (float) str_replace(',','.',$summ);
				if ($summ == 0) continue;
				//echo $phone." - $summ руб<br/>";
				$total_summ = $total_summ + $summ;
				
				
				
				$mysqli->query("
					INSERT INTO `data` SET
					`report_id`=$report_id,
					`operator_id`=$operator_id,
					`file_id`=$file_id,
					`phone`='$phone',
					`summ`='$summ'
				");
			
			}
			
			
		
		} else if ($type == 1) { // недельный отчет
			
			foreach ($objPHPExcel->getWorksheetIterator() as $sheet) {
			
				$rows_count = $sheet->getHighestRow();
				$total_summ = 0;
				
				for ($row = 2; $row <= $rows_count; $row++) {
					
					$phone = $sheet->getCell('A' . $row)->getValue();
					$phone = substr($phone,1,strlen($phone));
					
					$summ = $sheet->getCell('O' . $row)->getValue();
					$summ = (float) str_replace(',','.',$summ);
					if ($summ == 0) continue;
					//echo $phone." - $summ руб<br/>";
					$total_summ = $total_summ + $summ;
					
					
					
					$mysqli->query("
						INSERT INTO `data` SET
						`report_id`=$report_id,
						`operator_id`=$operator_id,
						`file_id`=$file_id,
						`phone`='$phone',
						`summ`='$summ'
					");
				
				}
			
			}
		}

	}
	
	
	
	// МЕГАФОН
	function file_load_3($file_id,$mysqli,$objPHPExcel,$report_id,$operator_id, $type) { 
		
		$rows_count = count($objPHPExcel);
		$total_summ = 0;
		
		if ($type == 0) { // обработка месячного отчета
			
			// сейчас BH 11 первый итого у симки
			for ($row = 11; $row <= $rows_count; $row++) {
				
				$phone = $objPHPExcel[$row]['A'];
				if (!is_numeric($phone)) continue;
				$summ = $objPHPExcel[$row]['BH'];
				$summ = (float) str_replace(',','.',$summ);
				if ($summ == 0) continue;
				//echo $phone." - $summ руб<br/>";
				$total_summ = $total_summ + $summ;
				
				// Проверяем, добавлены ли уже данные по мегафону, если добавлены, то суммируем
				$sql_check = $mysqli->query("
					SELECT * FROM `data` WHERE
					`report_id`=$report_id and
					`operator_id`=$operator_id and
					`file_id`=$file_id and
					`phone`='$phone'
				");
				$col_check = $sql_check->num_rows;
				if ($col_check == 0) {
					$mysqli->query("
						INSERT INTO `data` SET
						`report_id`=$report_id,
						`operator_id`=$operator_id,
						`file_id`=$file_id,
						`phone`='$phone',
						`summ`='$summ'
					");
					$data_id = $mysqli->insert_id;
				} else {
					$res_check = $sql_check->fetch_array();
					$summ = $res_check['summ'] + $summ;
					$mysqli->query("
						UPDATE `data` SET
						`summ`='$summ'
						WHERE
						`report_id`=$report_id and
						`operator_id`=$operator_id and
						`file_id`=$file_id and
						`phone`='$phone'
					");
					$data_id = $res_check['data_id'];
				}
				
				
				// Получаем полные данные по расходам мегафона. Названия переменных соответствует колонкам в отчете мегафона
				$D = $objPHPExcel[$row]['D'];
				$F = $objPHPExcel[$row]['F'];
				$H = $objPHPExcel[$row]['H'];
				$J = $objPHPExcel[$row]['J'];
				$N = $objPHPExcel[$row]['N'];
				$S = $objPHPExcel[$row]['S'];
				$X = $objPHPExcel[$row]['X'];
				$AC = $objPHPExcel[$row]['AC'];
				$AE = $objPHPExcel[$row]['AE'];
				$AI_2 = $objPHPExcel[$row]['AI'];
				$AK = $objPHPExcel[$row]['AK'];
				$AO = $objPHPExcel[$row]['AO'];
				$AQ = $objPHPExcel[$row]['AQ'];
				$AT = $objPHPExcel[$row]['AT'];
				$AV = $objPHPExcel[$row]['AV'];
				$AX = $objPHPExcel[$row]['AX'];
				$BA = $objPHPExcel[$row]['BA'];
				// теперь итого храниться в BH
				$BC = $objPHPExcel[$row]['BH'];

				$D = (double) str_replace(',','.',$D);
				$F = (double) str_replace(',','.',$F);
				$H = (double) str_replace(',','.',$H);
				$J = (double) str_replace(',','.',$J);
				$N = (double) str_replace(',','.',$N);
				$S = (double) str_replace(',','.',$S);
				$X = (double) str_replace(',','.',$X);
				$AC = (double) str_replace(',','.',$AC);
				$AE = (double) str_replace(',','.',$AE);
				$AI_2 = (double) str_replace(',','.',$AI_2);
				$AK = (double) str_replace(',','.',$AK);
				$AO = (double) str_replace(',','.',$AO);
				$AQ = (double) str_replace(',','.',$AQ);
				$AT = (double) str_replace(',','.',$AT);
				$AV = (double) str_replace(',','.',$AV);
				$AX = (double) str_replace(',','.',$AX);
				$BA = (double) str_replace(',','.',$BA);
				$BC = (double) str_replace(',','.',$BC);
				
				$sql_check = $mysqli->query("
					SELECT * FROM `data_megafon` WHERE
					`data_id`='$data_id' and
					`report_id`='$report_id'
				");
				
				
				// Проверяем добавлен ли в базу такой номер телефона, если добавлен, суммируем расходы
				$col_check = $sql_check->num_rows;
				if ($col_check == 0) {
				
					$mysqli->query("
						INSERT INTO `data_megafon` SET
						`data_id`='$data_id',
						`report_id`='$report_id',
						`D`='$D',
						`F`='$F',
						`H`='$H',
						`J`='$J',
						`N`='$N',
						`S`='$S',
						`X`='$X',
						`AC`='$AC',
						`AE`='$AE',
						`AI_2`='$AI_2',
						`AK`='$AK',
						`AO`='$AO',
						`AQ`='$AQ',
						`AT`='$AT',
						`AV`='$AV',
						`AX`='$AX',
						`BA`='$BA',
						`BC`='$BC'
					");
				
				} else {
					
					$res_check = $sql_check->fetch_array();
					
					$D = $D + $res_check['D'];
					$F = $F + $res_check['F'];
					$H = $H + $res_check['H'];
					$J = $J + $res_check['J'];
					$N = $N + $res_check['N'];
					$S = $S + $res_check['S'];
					$X = $X + $res_check['X'];
					$AC = $AC + $res_check['AC'];
					$AE = $AE + $res_check['AE'];
					$AI_2 = $AI_2 + $res_check['AI_2'];
					$AK = $AK + $res_check['AK'];
					$AO = $AO + $res_check['AO'];
					$AQ = $AQ + $res_check['AQ'];
					$AT = $AT + $res_check['AT'];
					$AV = $AV + $res_check['AV'];
					$AX = $AX + $res_check['AX'];
					$BA = $BA + $res_check['BA'];
					$BC = $BC + $res_check['BC'];
					
					$mysqli->query("
						UPDATE `data_megafon` SET
							`D`='$D',
							`F`='$F',
							`H`='$H',
							`J`='$J',
							`N`='$N',
							`S`='$S',
							`X`='$X',
							`AC`='$AC',
							`AE`='$AE',
							`AI_2`='$AI_2',
							`AK`='$AK',
							`AO`='$AO',
							`AQ`='$AQ',
							`AT`='$AT',
							`AV`='$AV',
							`AX`='$AX',
							`BA`='$BA',
							`BC`='$BC'
						WHERE
							`data_id`='$data_id' and
							`report_id`='$report_id'
					");
				}
			}
			
		} else if ($type == 1) { // Недельный отчет
			
			foreach ($objPHPExcel->getWorksheetIterator() as $sheet) {
				
				for ($row = 2; $row <= $sheet->getHighestRow(); $row++) {
					
					$phone = $sheet->getCell('A' . $row)->getValue();
					$phone = trim($phone);
					if (!is_numeric($phone)) continue;
					
					$summ = 0;
					
					$mysqli->query("
						INSERT INTO `data` SET
						`report_id`=$report_id,
						`operator_id`=$operator_id,
						`file_id`=$file_id,
						`phone`='$phone',
						`summ`='$summ'
					");
				}
				
			}

			
		}

	}
	
	
	// ТЕЛЕ 2
	function file_load_4($file_id,$mysqli,$objPHPExcel,$report_id,$operator_id, $type) { 
		
		
		foreach ($objPHPExcel->getWorksheetIterator() as $sheet) {
			
			$rows_count = $sheet->getHighestRow();
			$total_summ = 0;
			
			if ($type == 0) { // Месячный отчет
				for ($row = 5; $row <= $rows_count; $row++) {
					
					$phone = $sheet->getCell('A' . $row)->getValue();
					$phone = substr($phone,1,strlen($phone));
					if (!is_numeric($phone)) continue;
					$summ = $sheet->getCell('H' . $row)->getValue();
					$summ = (float) str_replace(',','.',$summ);
					if ($summ == 0) continue;
					//echo $phone." - $summ руб<br/>";
					$total_summ = $total_summ + $summ;
					
					$mysqli->query("
						INSERT INTO `data` SET
						`report_id`=$report_id,
						`operator_id`=$operator_id,
						`file_id`=$file_id,
						`phone`='$phone',
						`summ`='$summ'
					");
					
				}
				
			} else if ($type == 1) { // недельный отчет
				
				for ($row = 1; $row <= $rows_count; $row++) {
					
					$phone = $sheet->getCell('A' . $row)->getValue();
					$phone = str_replace(array('+7','-',' '),'',$phone);

					$mysqli->query("
						INSERT INTO `data` SET
						`report_id`=$report_id,
						`operator_id`=$operator_id,
						`file_id`=$file_id,
						`phone`='$phone'
					");
					
				}
				
			}

		}
		
		
	}
	
	// ЛЕТАЙ
	function file_load_2($file_id,$mysqli,$objPHPExcel,$report_id,$operator_id, $type) { 
		 
		foreach ($objPHPExcel->getWorksheetIterator() as $sheet) {
			
			$rows_count = $sheet->getHighestRow();
			$total_summ = 0;
			
			if ($type == 0) { // месячный отчет
			
				// Берем ячейку А6 и пытаемся из неё забрать текущий месяц.
				$a6 = $sheet->getCell('A6')->getValue();
				$a6_arr = explode('=',$a6);
				if (count($a6_arr) == 2) {
					$a6_arr_param = explode(' ',trim($a6_arr[1]));
					if (count($a6_arr_param) == 2) {
						$month = get_num_month_by_name(trim($a6_arr_param[0]));
						$year = $a6_arr_param[1];

						if (is_numeric($month) and $month != 0 and is_numeric($year)) {
							$mysqli->query("UPDATE `report` SET `month`='$month', `year`='$year' WHERE `report_id`=$report_id");
						}
					}
				}
				
				for ($row = 12; $row <= $rows_count; $row++) {
					
					$phone = $sheet->getCell('F' . $row)->getValue();
					$summ = $sheet->getCell('N' . $row)->getValue();
					$summ = (double) str_replace(',','.',$summ);
					$total_summ = $total_summ + $summ;
				
					$mysqli->query("
						INSERT INTO `data` SET
						`report_id`=$report_id,
						`operator_id`=$operator_id,
						`file_id`=$file_id,
						`phone`='$phone',
						`summ`='$summ'
					");
					
				}
				
			} else if ($type == 1) { // Недельный отчет
		
				for ($row = 2; $row <= $rows_count; $row++) {
					
					// Недельный отчет в формате CSV, поэтому берем строку и разбиваем по ;
					$one_row = $sheet->getCell('A' . $row)->getValue();
					$arr = explode(';',$one_row);
					
					$block = $arr[5];
					$phone = $arr[3];
					 
					if ($block == "Финансовая блокировка") {
						$phone = substr($phone,1,strlen($phone));
						$mysqli->query("
							INSERT INTO `data` SET
							`report_id`=$report_id,
							`operator_id`=$operator_id,
							`file_id`=$file_id,
							`phone`='$phone'
						");
					}
					
				}

			}

		}
		  
	}
	
	
	
	
	// СОТРУДНИКИ
	function file_load_5($file_id,$mysqli,$objPHPExcel,$report_id,$operator_id, $type) { 
		
		
		foreach ($objPHPExcel->getWorksheetIterator() as $sheet) {
			
			$rows_count = $sheet->getHighestRow();
			$total_summ = 0;
			
			for ($row = 1; $row <= $rows_count; $row++) {
				
				$phone_cell = $sheet->getCell('A' . $row)->getValue();
				$phone = preg_replace('/[^0-9]/', '', $phone_cell);
				if (!is_numeric($phone) or strlen($phone) != 10) continue;
				
				$name = $sheet->getCell('B' . $row)->getValue();
				
				//echo $phone." - ".$name."<br/>";
				
				$mysqli->query("
					INSERT INTO `staff` SET
					`report_id`='$report_id',
					`phone`='$phone',
					`phone_cell`='$phone_cell',
					`name`='$name'
				");
			}
		}
		
		
	}
	
	
	function get_operator_merchant($report_id,$operator_id,$mysqli) {
		
		$total_summ = 0;
		$total_ob = 0;
		$array = [];
		$i = 0;
		
		$sql_data = $mysqli->query("SELECT * FROM `data` WHERE `report_id`=$report_id and `operator_id`=$operator_id ORDER BY `summ` desc");
		while ($res_data = $sql_data->fetch_assoc()) {
			
			$phone = $res_data['phone'];
			$summ = $res_data['summ'];
			
			$sql_check_staff = $mysqli->query("
				SELECT * FROM `staff` WHERE `phone`='$phone' and `report_id`=$report_id
			");
			$col_check_staff = $sql_check_staff->num_rows;
			
			
			$sql_merchant = $mysqli->query("
				SELECT * FROM `merchant` WHERE (`s1`='$phone' or `s2`='$phone') and `report_id`=$report_id
			");
			$col_merchant = $sql_merchant->num_rows;
			if ($col_merchant > 0) {
				$res_merchant = $sql_merchant->fetch_array();
				$as = $res_merchant['as'];
				$on = $res_merchant['on'];
				$cn = $res_merchant['cn'];
			} else {
				$as = "";
				$on = "";
				$cn = "";
			}
			
			if ($col_check_staff == 0) {
				$array['data'][$i]['phone'] = $phone;
				$array['data'][$i]['summ'] = $summ;
				$array['data'][$i]['as'] = $as;
				$array['data'][$i]['on'] = $on;
				$array['data'][$i]['cn'] = $cn;
				$total_ob = $total_ob + $summ;
				$i++;
			}
			
			$total_summ = $total_summ + $summ;

		}
		
		$array['total_ob'] = $total_ob;
		$array['total_summ'] = $total_summ;
		
		$mysqli->query("UPDATE `report_file` SET `total_ob`='$total_ob', `total_summ`='$total_summ' WHERE `report_id`=$report_id and `operator_id`=$operator_id");
		
		return $array;
		
	}
	
	function get_num_month_by_name($name) {
		
		$m = 0;
		if ($name == "Январь") $m = 1;
		elseif ($name == "Февраль") $m = 2;
		elseif ($name == "Март") $m = 3;
		elseif ($name == "Апрель") $m = 4;
		elseif ($name == "Май") $m = 5;
		elseif ($name == "Июнь") $m = 6;
		elseif ($name == "Июль") $m = 7;
		elseif ($name == "Август") $m = 8;
		elseif ($name == "Сентябрь") $m = 9;
		elseif ($name == "Октябрь") $m = 10;
		elseif ($name == "Ноябрь") $m = 11;
		elseif ($name == "Декабрь") $m = 12;
		
		return $m;
		
	}
	
	function get_name_month_by_num($num) {
		
		if ($num == 1) $name = "Январь";
		elseif ($num == 2) $name = "Февраль";
		elseif ($num == 3) $name = "Март";
		elseif ($num == 4) $name = "Апрель";
		elseif ($num == 5) $name = "Май";
		elseif ($num == 6) $name = "Июнь";
		elseif ($num == 7) $name = "Июль";
		elseif ($num == 8) $name = "Август";
		elseif ($num == 9) $name = "Сентябрь";
		elseif ($num == 10) $name = "Октябрь";
		elseif ($num == 11) $name = "Ноябрь";
		elseif ($num == 12) $name = "Декабрь";
		else $name = "";
		
		return $name;
		
	}
	
	
	
	function get_array_merchant($report_id,$mysqli) {
		
		$big_total = 0;
		$big_total_sim1 = 0;
		$big_total_sim2 = 0;
		
		$array = [];
		$array_empty = [];
		$i = 0;
		$sql_merchant = $mysqli->query("SELECT * FROM `merchant` WHERE `report_id`=$report_id ORDER BY `merchant_id`");
		while ($res_merchant = $sql_merchant->fetch_assoc()) {
			
			$as = $res_merchant['as'];
			$on = $res_merchant['on'];
			$cn = $res_merchant['cn'];
			$merchant_id = $res_merchant['merchant_id'];
			$sim1 = $res_merchant['s1'];
			$sim2 = $res_merchant['s2'];
			
			$sql_as = $mysqli->query("SELECT * FROM `as` WHERE `key`='$as'");
			$col_as = $sql_as->num_rows;
			if ($col_as > 0) {
				
				$res_as = $sql_as->fetch_array();
				$as = $res_as['value'];
				
			}
			
			$total_summ_sim1 = 0;
			if ($sim1 != "" and $sim1 != '0') {
				$sql_report_file = $mysqli->query("SELECT * FROM `report_file` WHERE `report_id`=$report_id");
				while ($res_report_file = $sql_report_file->fetch_assoc()) {
					
					$file_id = $res_report_file['file_id'];
					$operator_id = $res_report_file['operator_id'];
					
					$sql_data = $mysqli->query("SELECT * FROM `data` WHERE `file_id`=$file_id and `phone`='$sim1'");
					$col_data = $sql_data->num_rows;
					if ($col_data > 0) {
						while ($res_data = $sql_data->fetch_assoc()) {
							
							$total_summ_sim1 = $total_summ_sim1 + $res_data['summ'];
							
						}
					}
				}
			}
			
			$total_summ_sim2 = 0;
			if ($sim2 != "" and $sim2 != '0') {
				$sql_report_file = $mysqli->query("SELECT * FROM `report_file` WHERE `report_id`=$report_id");
				while ($res_report_file = $sql_report_file->fetch_assoc()) {
					
					$file_id = $res_report_file['file_id'];
					$operator_id = $res_report_file['operator_id'];
					
					$sql_data = $mysqli->query("SELECT * FROM `data` WHERE `file_id`=$file_id and `phone`='$sim2'");
					$col_data = $sql_data->num_rows;
					if ($col_data > 0) {
						while ($res_data = $sql_data->fetch_assoc()) {
							
							$total_summ_sim2 = $total_summ_sim2 + $res_data['summ'];
							
						}
					}
				}
			}
			
			if ($total_summ_sim2 != 0 or $total_summ_sim1 != 0) {
				
				$array[$i]['as'] = $as;
				$array[$i]['on'] = $on;
				$array[$i]['cn'] = $cn;
				$array[$i]['sim1'] = $sim1;
				$array[$i]['sim2'] = $sim2;
				$array[$i]['sim1_summ'] = $total_summ_sim1;
				$array[$i]['sim2_summ'] = $total_summ_sim2;
				$array[$i]['total_summ'] = $total_summ_sim2 + $total_summ_sim1;
				
				$big_total = $big_total + $total_summ_sim2 + $total_summ_sim1;
				$big_total_sim1 = $big_total_sim1 + $total_summ_sim1;
				$big_total_sim2 = $big_total_sim2 + $total_summ_sim2;
				
				$i++;
			}
		
		}
		
		/*echo $big_total."<br/>";
		echo $big_total_sim1."<br/>";
		echo $big_total_sim2."<br/>";
		
		echo "<pre>";
		print_r($array);
		echo "</pre>";
		
		die();*/
		
		return $array;
	
	}
	
	
	
	function get_array_merchant_full($report_id,$mysqli) {
		
		$big_total = 0;
		$big_total_sim1 = 0;
		$big_total_sim2 = 0;
		
		$array = [];
		$array_empty = [];
		$i = 0;
		$sql_merchant = $mysqli->query("SELECT * FROM `merchant` WHERE `report_id`=$report_id ORDER BY `merchant_id`");
		while ($res_merchant = $sql_merchant->fetch_assoc()) {
			
			$as = $res_merchant['as'];
			$on = $res_merchant['on'];
			$cn = $res_merchant['cn'];
			$merchant_id = $res_merchant['merchant_id'];
			$sim1 = $res_merchant['s1'];
			$sim2 = $res_merchant['s2'];
			
			$sql_as = $mysqli->query("SELECT * FROM `as` WHERE `key`='$as'");
			$col_as = $sql_as->num_rows;
			if ($col_as > 0) {
				
				$res_as = $sql_as->fetch_array();
				$as = $res_as['value'];
				
			}
			
			$total_summ_sim1 = 0;
			if ($sim1 != "" and $sim1 != '0') {
				$sql_report_file = $mysqli->query("SELECT * FROM `report_file` WHERE `report_id`=$report_id");
				while ($res_report_file = $sql_report_file->fetch_assoc()) {
					
					$file_id = $res_report_file['file_id'];
					$operator_id = $res_report_file['operator_id'];
					
					$sql_data = $mysqli->query("SELECT * FROM `data` WHERE `file_id`=$file_id and `phone`='$sim1'");
					$col_data = $sql_data->num_rows;
					if ($col_data > 0) {
						while ($res_data = $sql_data->fetch_assoc()) {
							
							$total_summ_sim1 = $total_summ_sim1 + $res_data['summ'];
							
						}
					}
				}
			}
			
			$total_summ_sim2 = 0;
			if ($sim2 != "" and $sim2 != '0') {
				$sql_report_file = $mysqli->query("SELECT * FROM `report_file` WHERE `report_id`=$report_id");
				while ($res_report_file = $sql_report_file->fetch_assoc()) {
					
					$file_id = $res_report_file['file_id'];
					$operator_id = $res_report_file['operator_id'];
					
					$sql_data = $mysqli->query("SELECT * FROM `data` WHERE `file_id`=$file_id and `phone`='$sim2'");
					$col_data = $sql_data->num_rows;
					if ($col_data > 0) {
						while ($res_data = $sql_data->fetch_assoc()) {
							
							$total_summ_sim2 = $total_summ_sim2 + $res_data['summ'];
							
						}
					}
				}
			}
			
			if ($total_summ_sim2 != 0 or $total_summ_sim1 != 0) {
				
				$array[$i]['as'] = $as;
				$array[$i]['on'] = $on;
				$array[$i]['cn'] = $cn;
				$array[$i]['sim1'] = $sim1;
				$array[$i]['sim2'] = $sim2;
				$array[$i]['sim1_summ'] = $total_summ_sim1;
				$array[$i]['sim2_summ'] = $total_summ_sim2;
				$array[$i]['total_summ'] = $total_summ_sim2 + $total_summ_sim1;
				
				$big_total = $big_total + $total_summ_sim2 + $total_summ_sim1;
				$big_total_sim1 = $big_total_sim1 + $total_summ_sim1;
				$big_total_sim2 = $big_total_sim2 + $total_summ_sim2;
				
				$i++;
				
			}
			
			if ($sim1 == 0 and $sim2 == 0) {
			
				$array[$i]['as'] = $as;
				$array[$i]['on'] = $on;
				$array[$i]['cn'] = $cn;
				$array[$i]['sim1'] = $sim1;
				$array[$i]['sim2'] = $sim2;
				$array[$i]['sim1_summ'] = 0;
				$array[$i]['sim2_summ'] = 0;
				$array[$i]['total_summ'] = 0;
				
				$i++;
				
			}

		}
		
		/*echo $big_total."<br/>";
		echo $big_total_sim1."<br/>";
		echo $big_total_sim2."<br/>";
		
		echo "<pre>";
		print_r($array);
		echo "</pre>";
		
		die();*/
		
		return $array;
	
	}
	
	
	function get_array_merchant_block($report_id,$mysqli) {
	
		$array = [];
		$i = 0;
		
		$sql_data = $mysqli->query("SELECT * FROM `data` WHERE `report_id`=$report_id");
		$col_data = $sql_data->num_rows;
		if ($col_data > 0) {
			while ($res_data = $sql_data->fetch_assoc()) {
				
				$phone = $res_data['phone'];
				$summ = $res_data['summ'];
				$operator_id = $res_data['operator_id'];
				
				if ($summ == 0) $summ = "";
				
				$sql_operator = $mysqli->query("SELECT * FROM `operator` WHERE `operator_id`=$operator_id");
				$res_operator = $sql_operator->fetch_array();
				
				$array[$i]['phone'] = $phone;
				$array[$i]['summ'] = $summ;
				$array[$i]['operator'] = $res_operator['name'];
				
				$sql_merchant = $mysqli->query("SELECT * FROM `merchant` WHERE `report_id`=$report_id and (`s1`='$phone' or `s2`='$phone')");
				$col_merchant = $sql_merchant->num_rows;
				if ($col_merchant > 0) {
					$res_merchant = $sql_merchant->fetch_array();
					$as = $res_merchant['as'];
					$on = $res_merchant['on'];
					$cn = $res_merchant['cn'];
					
					$sql_as = $mysqli->query("SELECT * FROM `as` WHERE `key`='$as'");
					$col_as = $sql_as->num_rows;
					if ($col_as > 0) {
						
						$res_as = $sql_as->fetch_array();
						$as = $res_as['value'];
						
					}
					
					$array[$i]['as'] = $as;
					$array[$i]['on'] = $on;
					$array[$i]['cn'] = $cn;
				} else {
					$array[$i]['as'] = "";
					$array[$i]['on'] = "";
					$array[$i]['cn'] = "";

				}
				
				$i++;
			}
		}
		
		
		return $array;
	
	}
	
	
	
	
	function get_array_staff($report_id,$mysqli) {
		
		$array = [];
		$i = 0;
		$total_summ = 0;
		
		$sql_staff = $mysqli->query("SELECT * FROM `staff` WHERE `report_id`=$report_id");
		while ($res_staff = $sql_staff->fetch_assoc()) {
			
			$phone = $res_staff['phone'];
			$phone_cell = $res_staff['phone_cell'];
			$name = $res_staff['name'];
			
			$summ = 0;
			
			$D = 0;
			$F = 0;
			$H = 0;
			$J = 0;
			$N = 0;
			$S = 0;
			$X = 0;
			$AC = 0;
			$AE = 0;
			$AI_2 = 0;
			$AK = 0;
			$AO = 0;
			$AQ = 0;
			$AT = 0;
			$AV = 0;
			$AX = 0;
			$BA = 0;
			$BC = 0;
			
			$sql_summ = $mysqli->query("SELECT * FROM `data` WHERE `report_id`=$report_id and `phone`='$phone'");
			$col_summ = $sql_summ->num_rows;
			if ($col_summ > 0) {
				$res_summ = $sql_summ->fetch_array();
				$summ = $res_summ['summ'];
				$data_id = $res_summ['data_id'];
				
				$sql_data_megafon = $mysqli->query("SELECT * FROM `data_megafon` WHERE `data_id`=$data_id");
				$col_data_megafon = $sql_data_megafon->num_rows;
				if ($col_data_megafon > 0) {
					
					$res_data_megafon = $sql_data_megafon->fetch_array();

					$D = $res_data_megafon['D'];
					$F = $res_data_megafon['F'];
					$H = $res_data_megafon['H'];
					$J = $res_data_megafon['J'];
					$N = $res_data_megafon['N'];
					$S = $res_data_megafon['S'];
					$X = $res_data_megafon['X'];
					$AC = $res_data_megafon['AC'];
					$AE = $res_data_megafon['AE'];
					$AI_2 = $res_data_megafon['AI_2'];
					$AK = $res_data_megafon['AK'];
					$AO = $res_data_megafon['AO'];
					$AQ = $res_data_megafon['AQ'];
					$AT = $res_data_megafon['AT'];
					$AV = $res_data_megafon['AV'];
					$AX = $res_data_megafon['AX'];
					$BA = $res_data_megafon['BA'];
					$BC = $res_data_megafon['BC'];

				}
			}
		

			$array['data'][$i]['phone_cell'] = $phone_cell;
			$array['data'][$i]['name'] = $name;
			
			$array['data'][$i]['D'] = $D;
			$array['data'][$i]['F'] = $F;
			$array['data'][$i]['H'] = $H;
			$array['data'][$i]['J'] = $J;
			$array['data'][$i]['N'] = $N;
			$array['data'][$i]['S'] = $S;
			$array['data'][$i]['X'] = $X;
			$array['data'][$i]['AC'] = $AC;
			$array['data'][$i]['AE'] = $AE;
			$array['data'][$i]['AI_2'] = $AI_2;
			$array['data'][$i]['AK'] = $AK;
			$array['data'][$i]['AO'] = $AO;
			$array['data'][$i]['AQ'] = $AQ;
			$array['data'][$i]['AT'] = $AT;
			$array['data'][$i]['AV'] = $AV;
			$array['data'][$i]['AX'] = $AX;
			$array['data'][$i]['BA'] = $BA;
			$array['data'][$i]['BC'] = $BC;

			
			$total_summ = $total_summ + $summ;
			$i++;
		}
		
		$array['total_summ'] = $total_summ;
		

		return $array;
	}
	
	
	
	function find_not_isset_in_merchant($report_id,$mysqli) {
		
		$array = array();
		$i = 0;
		
		$sql_data = $mysqli->query("SELECT * FROM `data` WHERE `report_id`=$report_id");
		while ($res_data = $sql_data->fetch_assoc()) {
			
			$phone = $res_data['phone'];
			$summ = $res_data['summ'];
			
			
			$sql_merchant = $mysqli->query("SELECT * FROM `merchant` WHERE (`s1`='$phone' or `s2`='$phone') and `report_id`=$report_id");
			$col_merchant = $sql_merchant->num_rows;
			
			if ($col_merchant == 0) {
				$array[$i]['phone'] = $phone;
				$array[$i]['summ'] = $summ;
				$i++;
			}
			
		}
		
		return $array;
	}
	
	
	
	
	
	
?>