<?php
	include('connect_db.php');
	include('function.php');
	
	if (!isset($_SESSION['user_id'])) header('location: /login.php');
	
	use PhpOffice\PhpSpreadsheet\Helper\Sample;
	use PhpOffice\PhpSpreadsheet\IOFactory;
	use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
	require_once __DIR__ . '/vendor/phpoffice/phpspreadsheet/src/Bootstrap.php';
	require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
	require_once dirname(__FILE__) . '/Classes/PHPExcel/IOFactory.php';

	
	$helper = new Sample();
	if ($helper->isCli()) {
		return;
	}
	
	
	if (isset($_GET['del']) and is_numeric($_GET['del'])) {
		
		$mysqli->query("DELETE FROM `report` WHERE `report_id`=".$_GET['del']);
		$mysqli->query("DELETE FROM `report_file` WHERE `report_id`=".$_GET['del']);
		$mysqli->query("DELETE FROM `data` WHERE `report_id`=".$_GET['del']);
		$mysqli->query("DELETE FROM `merchant` WHERE `report_id`=".$_GET['del']);
		$mysqli->query("DELETE FROM `staff` WHERE `report_id`=".$_GET['del']);
		$mysqli->query("DELETE FROM `data_megafon` WHERE `report_id`=".$_GET['del']);
		
		header('location: /');
	}
	
	
	// Действия после нажатия на ссылку копирования
	if (isset($_GET['copy']) and is_numeric($_GET['copy'])) {
		
		$sql_last = $mysqli->query("SELECT * FROM `report` WHERE `report_id`=".$_GET['copy']);
		$res_last = $sql_last->fetch_array();
		
		$new_date = date("d.m.Y");
		
		$type = $res_last['type'];
		$save = $res_last['save'];
		$name = $res_last['name'] . " (копия от $new_date)";
		
		$datetime = time();
		
		$mysqli->query("
			INSERT INTO `report` SET 
			`datetime`='$datetime', 
			`type`='$type', 
			`save`='$save', 
			`name`='$name'
		");
		
		$report_id = $mysqli->insert_id;
		
		$sql_report_file = $mysqli->query("SELECT * FROM `report_file` WHERE `report_id`=".$_GET['copy']);
		while ($res_report_file = $sql_report_file->fetch_assoc()) {
			$src = $res_report_file['src'];
			
			$operator_id = $res_report_file['operator_id'];
			$mysqli->query("
				INSERT INTO `report_file` SET
				`report_id`='$report_id',
				`src`='$src',
				`operator_id`='$operator_id'
			");
			
			$upload_dir = $res_report_file['src'];
			
			
			// Склеиваем два файла мегафона для отправки в обработку в одну функцию. Название функции зависит от operator_id
			if ($operator_id == 7) $operator_id = 3;
			
			
			if ($operator_id == 1 and $type == 0) { // Если МТС месячный отчет, то читаем читаем xls файл с добавлением setReadDataOnly
					
				$inputFileType = 'Xls';
				$reader = IOFactory::createReader($inputFileType);
				$reader->setReadDataOnly(true);
				$spreadsheet = $reader->load($upload_dir);
				$objPHPExcel = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
			
			} elseif (($operator_id == 7 or $operator_id == 3) and $type == 0) { // Если мегафон месячный отчет
				// Месячные отчеты мегафона читаются только так
				$reader = new Xlsx();
				$reader->setLoadSheetsOnly('Page4');
				$reader->setReadDataOnly(true);
				$spreadsheet = $reader->load($upload_dir);
				$objPHPExcel = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
				
			} else	{
			
				$objPHPExcel = PHPExcel_IOFactory::load($upload_dir);
			
			}
			
			
			$file_id = $mysqli->insert_id;
			
			$name_function = "file_load_".$operator_id;
			
			// Функция обработки загруженных файлов. Все данные записываются в базу.
			$name_function($file_id, $mysqli, $objPHPExcel,$report_id,$operator_id, $type);
		
		}
		
		// Обновление json файлы с сайта evroadmin.ru
		update_json($mysqli, $report_id);
		
		
		header('location: /');
	}
	
	
	// Действия после добавления нового отчета
	if (isset($_POST['sb_add'])) {
		
		$datetime = time();
		$name = to_db($_POST['name'],$mysqli); // название отчета
		$type = to_db($_POST['type'],$mysqli); // недельный или месячный
		if (isset($_POST['save'])) $save = 1;
		else $save = 0;
		
		$mysqli->query("
			INSERT INTO `report` SET 
			`datetime`='$datetime', 
			`type`='$type', 
			`save`='$save', 
			`name`='$name'
		");
		$report_id = $mysqli->insert_id;
		
		// Обновляем json с сайта евроадмин
        // TODO убрать not_update после дебага!
		update_json($mysqli,$report_id, true);
		
		$have_file = 0;
		foreach ($_FILES as $key=>$operator) {
			
			$arr = explode("_", $key);
			$operator_id = $arr[1];
			
			for ($i = 0; $i < count($operator['name']); $i++) {
				if ($operator['name'][$i] != "") {
					$ext = getExtension($operator['name'][$i]);
					$tmp_name = $operator['tmp_name'][$i];
					$new_name_file = uniqid().".".$ext;
					$upload_dir = "excel/$new_name_file";
					move_uploaded_file($tmp_name,$upload_dir);
					
					// Добавляем в базу данные по ссылке на загруженный файл отчета и id оператора
					$mysqli->query("
						INSERT INTO `report_file` SET 
						`src`='$upload_dir', 
						`report_id`=$report_id, 
						`operator_id`=$operator_id
					");
					
					$have_file++;
					
					
					// type = 0 - месячный отчет
					// type = 1 - недельный отчет
				
					
					// Читаем файлы и передаем в функцию переменную objPHPExcel. Где-то переменная передается как массив, а где-то как объект. Каша малаша получилась из-за того, что некоторые файлы обычным путем не читаются. Поэтому в коде используется библиотека PhpSpreadSheet, а также phpexcel (это одно и тоже но разные версии, я начинал делать с PhpExcel, но его оказалось недостаточно)
					
					if ($operator_id == 1 and $type == 0) { // Если месячный отчет от МТС, то читаем по особому
					
						$inputFileType = 'Xls';
						$reader = IOFactory::createReader($inputFileType);
						$reader->setReadDataOnly(true);
						$spreadsheet = $reader->load($upload_dir);
						$objPHPExcel = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
					
					} elseif (($operator_id == 7 or $operator_id == 3) and $type == 0) { // Если месячный отчет от мегафона
						// Месячные отчеты мегафона читаются только так
						$reader = new Xlsx();
						$reader->setLoadSheetsOnly('Page4'); // Берем только лист с Page4
						$reader->setReadDataOnly(true);
						$spreadsheet = $reader->load($upload_dir);
						$objPHPExcel = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
						
					} else	{
					
						$objPHPExcel = PHPExcel_IOFactory::load($upload_dir);
					
					}
					
					
					$file_id = $mysqli->insert_id;
					
					
					// Склеиваем два файла мегафона для отправки в обработку в одну функцию
					if ($operator_id == 7) $operator_id = 3;
					
					// Отправляем в функию отчет для обработки
					$name_function = "file_load_".$operator_id;
					$name_function($file_id, $mysqli, $objPHPExcel,$report_id,$operator_id, $type);
				
				}
			}
			
		}
		
		if ($have_file == 0) {
			$mysqli->query("DELETE FROM `report` WHERE `report_id`=$report_id");
		}
		

	}	
?>
<!DOCTYPE html>
<html>
	<head> 
		<title>Обработка EXEL</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
		<META NAME="description" CONTENT=""/>
		<link rel="SHORTCUT ICON" href="images/1.ico"type="image/x-icon"/>
		<META Name="keywords" Content=""/>
		<meta http-equiv="Content-Language" content="ru-RU"/>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<link rel="stylesheet" href="/style.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	</head>

	<body>
		<?php include('header.php'); ?>	
		
		<div class="conteiner-fluid p-4">
		
			<h4>Создание отчета</h4>
		
			<form class="row bg_grey mt-4 mx-1 py-4 px-2" action="" method="POST" enctype="multipart/form-data">
				
				<?php
					$sql_operator = $mysqli->query("SELECT * FROM `operator` ORDER BY `sort`");
					while ($res_operator = $sql_operator->fetch_assoc()) {
				?>
						<div class="col-3">
							<div class="mt-2"><strong><?=$res_operator['name'];?></strong></div>
							<div class="mt-2">
								<input type="file" class="form-control" name="file_<?=$res_operator['operator_id'];?>[]" multiple />
							</div>
						</div>
				<?php
					}
				?>
				
				
				<div class="col-3">
					<div class="mt-2"><strong>Название отчета</strong></div>
					<div class="mt-2">
						<input type="text" name="name" value="Отчет от <?=date('d.m.Y H:i');?>" class="form-control"/>
					</div>
				</div>
				
				<div class="col-3">
					<div class="mt-2"><strong>Тип отчета</strong></div>
					<div class="mt-2">
						<select name="type" class="form-control">
							<option value="0">Месячный отчет</option>
							<option value="1" selected>Недельный отчет</option>
						</select>
					</div>
				</div>
				
				<div class="col-3 d-none">
					<div class="mt-2"><strong>Сохранять отчет</strong></div>
					<div class="mt-2">
						<div class="my-auto">
							<div class="custom-control custom-checkbox">
								<input name="save" value="1" type="checkbox" id="save" class="custom-control-input">
								<label class="custom-control-label" for="save"> Сохранить отчет</label>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-3">
					
					<div class="mt-2"><strong>&nbsp;</strong></div>
					<div class="mt-2">
						<input name="sb_add" type="submit" value="Сформировать отчет" class="form-control btn btn-success"/>
					</div>
				</div>
			</form>
			
			<br/>
			
			<h4>Сформированные отчеты</h4>
			<br/>
			<table class="table">
				<thead>
					<tr>
						<th>Дата</th>
						<th>Название отчета</th>
						<th class="d-none"></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					
					
				<?php
					$sql_report = $mysqli->query("SELECT * FROM `report` ORDER BY `report_id` desc");
					while ($res_report = $sql_report->fetch_assoc()) {
						$report_id = $res_report['report_id'];
				?>
						<tr>
							<td><?=date('d.m.Y в H:i', $res_report['datetime']);?></td>
							<td>
								<div class="pb-1"><?=$res_report['name'];?></div>
								<?php
									$sql_report_file = $mysqli->query("SELECT * FROM `report_file` WHERE `report_id`=$report_id");
									while ($res_report_file = $sql_report_file->fetch_assoc()) {
										
										$operator_id = $res_report_file['operator_id'];
										$sql_operator = $mysqli->query("SELECT * FROM `operator` WHERE `operator_id`=$operator_id");
										$res_operator = $sql_operator->fetch_array();
								?>
											
										<div style="font-size: 12px; color: rgb(100,100,100);">
											<?=$res_operator['name'];?>: 
											<a href="/<?=$res_report_file['src'];?>"><?=$res_report_file['src'];?></a>
											<?php /*if ($operator_id == 5) { ?>
											<a href="/staff.php?report_id=<?=$res_report['report_id'];?>">[&nbsp;отчет&nbsp;]</a></div>
											<?php } else { ?>
											<a href="/operator.php?operator_id=<?=$operator_id;?>&report_id=<?=$res_report['report_id'];?>">[&nbsp;отчет&nbsp;]</a></div>
											<?php }*/ ?>
								<?php	
									}
								?>
								
							</td>
							<td class="d-none"><a href="report.php?report_id=<?=$res_report['report_id'];?>">[&nbsp;смотреть&nbsp;отчет&nbsp;]</a></td>
							
							<?php if ($res_report['excel_final'] == "") { ?>
							<td><a href="download_final.php?report_id=<?=$res_report['report_id'];?>">[&nbsp;месячный&nbsp;отчет&nbsp;№2&nbsp;]</a></td>
							<?php } else { ?>
							<td><a href="<?=$res_report['excel_final'];?>">[&nbsp;месячный&nbsp;отчет&nbsp;№2&nbsp;]</a></td>
							<?php } ?>
							<td><a href="download_week.php?report_id=<?=$res_report['report_id'];?>">[&nbsp;недельный&nbsp;отчет&nbsp;№3&nbsp;]</a></td>
							<td><a style="color: green;" href="index.php?copy=<?=$res_report['report_id'];?>">[&nbsp;копировать&nbsp;]</a></td>
							<td><a style="color: red;" href="index.php?del=<?=$res_report['report_id'];?>">[&nbsp;удалить&nbsp;]</a></td>
						</tr>
				<?php
						
					}
				?>
				
				</tbody>
			</table>
				
		
			
		</div>
		
		<?php include('footer.php'); ?>	

	</body>
</html>

		
		